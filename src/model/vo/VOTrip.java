package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip implements Comparable<VOTrip>
{

	private String tripId;
	private String startTime;
	private String endTime;
	private String bikeId;
	private String tripDuration;
	private String fromStationId;
	private String fromStationName;
	private String toStationId;
	private String toStationName;
	private String userType;
	private String gender;
	private String birthYear;

	public VOTrip(String tripId, String startTime, String endTime, String bikeId, String tripDuration,
			String fromStationId, String fromStationName, String toStationId, String toStationName, String userType,
			String gender, String birthYear) 
	{
		//super();
		this.tripId = tripId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.bikeId = bikeId;
		this.tripDuration = tripDuration;
		this.fromStationId = fromStationId;
		this.fromStationName = fromStationName;
		this.toStationId = toStationId;
		this.toStationName = toStationName;
		this.userType = userType;
		this.gender = gender;
		this.birthYear = birthYear;
	}


	public String darTripId() 
	{
		return tripId;
	}

	public void cambiarTripId(String tripId) 
	{
		this.tripId = tripId;
	}

	public String darStartTime() 
	{
		return startTime;
	}

	public void cambiarStartTime(String startTime) 
	{
		this.startTime = startTime;
	}

	public String darEndTime() 
	{
		return endTime;
	}

	public void cambiarEndTime(String endTime) 
	{
		this.endTime = endTime;
	}

	public String darBikeId() 
	{
		return bikeId;
	}

	public void cambiarBikeId(String bikeId) 
	{
		this.bikeId = bikeId;
	}

	public String darTripDuration() 
	{
		return tripDuration;
	}

	public void cambiarTripDuration(String tripDuration) 
	{
		this.tripDuration = tripDuration;
	}


	public String darFromStationId() 
	{
		return fromStationId;
	}

	public void cambiarFromStationId(String fromStationId) 
	{
		this.fromStationId = fromStationId;
	}

	public String darFromStationName() 
	{
		return fromStationName;
	}

	public void cambiarFromStationName(String fromStationName) 
	{
		this.fromStationName = fromStationName;
	}

	public String darToStationId() 
	{
		return toStationId;
	}

	public void cambiarToStationId(String toStationId) 
	{
		this.toStationId = toStationId;
	}

	public String darToStationName() 
	{
		return toStationName;
	}

	public void cambiarToStationName(String toStationName) 
	{
		this.toStationName = toStationName;
	}

	public String darUserType() 
	{
		return userType;
	}

	public void cambiarUserType(String userType) 
	{
		this.userType = userType;
	}

	public String darGender() 
	{
		return gender;
	}

	public void cambiarGender(String gender) 
	{
		this.gender = gender;
	}

	public String darBirthYear() 
	{
		return birthYear;
	}

	public void cambiarBirthYear(String birthYear) 
	{
		this.birthYear = birthYear;
	}

	@Override
	public int compareTo(VOTrip o) 
	{
		// TODO Auto-generated method stub
		return 0;
	}
}
