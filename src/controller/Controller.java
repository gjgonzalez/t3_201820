package controller;

import api.IDivvyTripsManager;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.IQueue;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller {
	
	public final static String RUTA_STATIONS ="./src/data/Divvy_Stations_2017_Q3Q4.csv" ;
	public final static String RUTA_TRIPS = "./src/data/Divvy_Trips_2017_Q3.csv";

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadStations() 
	{
		manager.loadStations(RUTA_STATIONS);
	}
	
	public static void loadTrips() 
	{
		manager.loadTrips(RUTA_TRIPS);
	}
		
	public static IQueue <String> getLastNStations (int bicycleId, int n) 
	{
		return manager.getLastNStations(bicycleId, n);
	}
	
	public static VOTrip customerNumberN (int stationID, int n) 
	{
		return manager.customerNumberN(stationID, n);
	}
}
