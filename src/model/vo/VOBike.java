package model.vo;

/**
 * Representation of a bike object
 */
public class VOBike implements Comparable<VOBike>
{

	private String id;
	private String name;
	private String city;
	private String latitude;
	private String longitude;
	private String dpCapacity;
	private String onlineDate;
	
	public VOBike(String id, String name, String city, String latitude, String longitude, String dpCapacity,
			String onlineDate) {
		super();
		this.id = id;
		this.name = name;
		this.city = city;
		this.latitude = latitude;
		this.longitude = longitude;
		this.dpCapacity = dpCapacity;
		this.onlineDate = onlineDate;
	}


	public String darId() 
	{
		return id;
	}


	public void cambiarId(String id) 
	{
		this.id = id;
	}


	public String darName() 
	{
		return name;
	}


	public void cambiarName(String name) 
	{
		this.name = name;
	}


	public String darCity() 
	{
		return city;
	}


	public void cambiarCity(String city) 
	{
		this.city = city;
	}


	public String darLatitude() 
	{
		return latitude;
	}


	public void cambiarLatitude(String latitude) 
	{
		this.latitude = latitude;
	}


	public String darLongitude() 
	{
		return longitude;
	}


	public void cambiarLongitude(String longitude) 
	{
		this.longitude = longitude;
	}


	public String darDpCapacity() 
	{
		return dpCapacity;
	}


	public void cambiarDpCapacity(String dpCapacity) 
	{
		this.dpCapacity = dpCapacity;
	}


	public String darOnlineDate() 
	{
		return onlineDate;
	}


	public void cambiarOnlineDate(String onlineDate) 
	{
		this.onlineDate = onlineDate;
	}

	@Override
	public int compareTo(VOBike o) {
		// TODO Auto-generated method stub
		return 0;
	}	
}
