package model.logic;

import java.io.FileReader;
import java.util.Iterator;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOBike;
import model.vo.VOTrip;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class DivvyTripsManager implements IDivvyTripsManager {

	private Queue<VOTrip> queueTrips = new Queue<>();
	private Queue<VOBike> queueStations = new Queue<>();
	private Stack<VOTrip> stackTrips = new Stack<>();
	private Stack<VOBike> stackStations = new Stack<>();
	
	public void loadStations (String stationsFile) 
	{
		
		try 
		{
			CSVReader reader = new CSVReader(new FileReader (stationsFile));
			
			String [] nextLine;
			reader.readNext();
			
			while((nextLine = reader.readNext()) != null)
			{
				VOBike station = new VOBike (nextLine[0], nextLine[1],nextLine[2],nextLine[3],nextLine[4],nextLine[5], nextLine[6]);
				queueStations.enqueue(station);
				stackStations.push(station);
			}
			
			System.out.println("Se creo la coleccion de estaciones");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.out.println("Se produjo un error. No se pudieron cargar los datos");
		}
		
	}
	
	public void loadTrips (String tripsFile) 
	{
		try 
		{
			CSVReader reader = new CSVReader (new FileReader (tripsFile));
			
			String [] nextLine;
			reader.readNext();
			
			while((nextLine = reader.readNext())!= null)
			{
				VOTrip trip = new VOTrip(nextLine[0], nextLine[1], nextLine[2], nextLine[3], nextLine[4], nextLine[5], nextLine[6], nextLine[7], nextLine[8], nextLine[9], nextLine[10], nextLine[11]);
				queueTrips.enqueue(trip);
				stackTrips.push(trip);
			}
			
			System.out.println(queueTrips.size());
			
			System.out.println("Se creo la coleccion de viajes");
			
			reader.close();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.out.println("Se produjo un error. No se pudieron cargar los viajes");
			System.out.println(e.getMessage());
		}
		
	}
	
	@Override
	public IQueue<String> getLastNStations (int bicycleId, int n) 
	{
		Queue<String>  lastNStations = new Queue<>();
		
		Iterator<VOTrip> iter = stackTrips.iterator();
		int contador = 0;
		
		while(iter.hasNext() && contador < n)
		{
			VOTrip trip = iter.next();
			
			if (trip.darBikeId().trim().equals(String.valueOf(bicycleId)))
			{
				lastNStations.enqueue(trip.darToStationName());
				contador++;
			}
		}
		
		return lastNStations;
	}

	@Override
	public VOTrip customerNumberN (int stationID, int n) 
	{
		VOTrip encontrado = null;
		
		Iterator<VOTrip> iter = queueTrips.iterator();
		
		int contador =0;
		
		while (iter.hasNext() && contador < n)
		{
			VOTrip trip = iter.next();
			
			if (trip.darToStationId().trim().equals(String.valueOf(stationID)))
			{
				contador ++;
				
				if (contador == n)
				{
					encontrado = trip;
				}
			}		
		}	
		return encontrado;
	}	
}
