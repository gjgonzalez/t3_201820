package tests.model.data.structuresa;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.data_structures.Stack;

public class StackTest<T extends Comparable<T>>
{
	private Stack<T> st;
	public static final String obj1 = "obj1";
	public static final String obj2 = "obj2";
	public static final String obj3 = "obj3";
	public static final String obj4 = "obj4";
	public static final String obj5 = "obj5";
	
	public void setUp() throws Exception 
 	{
        st = new Stack<T>();
    }
 
    @SuppressWarnings("unchecked")
	private void pushObjectsInOrder(String... objects) 
    {
        for (String str : objects) 
        {
            st.push((T) str);
        }
    }
	
	@Test
	public void testSizeCero()
	{
		try
		{
			setUp();
			assertEquals(0, st.size());
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSize()
	{
		try 
		{
			setUp();
			pushObjectsInOrder(obj1, obj2);
			assertEquals(2,st.size());
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testPush()
	{
		try
		{
			setUp();
			pushObjectsInOrder(obj1, obj2, obj3, obj4);
			st.push((T)obj5);
	        assertEquals(5, st.size());
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testPop()
	{
		try
		{
			setUp();
			pushObjectsInOrder(obj1, obj2, obj3, obj4, obj5);
			st.pop();
	        assertEquals(4, st.size());
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public void testIsEmpty()
	{
		try
		{
			setUp();
			st.isEmpty();
			assertTrue("Deberia estar vacio", true);
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
}


