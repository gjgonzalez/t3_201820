package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoublyLinkedList<T> extends Iterable<T> 
{

	//TODO Agregar API de DoublyLinkedList
	public int getSize();
	
	public void add( T element );
	public void addAtEnd( T element );
	public boolean addAtK( T element, int k );
	public T getElement(T element);
	public T getCurrentElement(T element);
	public void delete(T element);
	public boolean deleteAtK(int k);
	public void next();
	public void previous();

}
